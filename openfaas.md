## TP Final
### Laboratorio de Sistemas Operativos y Redes
### Proyecto: OpenFaas
### Integrantes: Emilio Alvarez

### Introducción

### Arquitecturas serverless
Las arquitecturas serverless son diseños de aplicaciones que incorporan servicios “Backend as a Service” (BaaS) de terceros y / o que incluyen código personalizado ejecutado en contenedores administrados y efímeros en una plataforma de “Funciones como servicio” (FaaS). Al usar estas ideas y otras relacionadas, como las aplicaciones de una sola página, tales arquitecturas eliminan gran parte de la necesidad de un componente de servidor tradicional siempre activo. 
Las arquitecturas "sin servidor" pueden beneficiarse de un costo operativo significativamente reducido, complejidad y tiempo de entrega de ingeniería, a un costo de mayor dependencia de las dependencias del proveedor y servicios de soporte relativamente inmaduros. 

#### ¿Qué es OpenFaas?
OpenFaas es una plataforma Faas (funciones como servicio), montada sobre Kubernetes usando contenedores de Docker para almacenar y ejecutar funciones. Permite encapsular programas en un contenedor, administrados como funciones, las cuales pueden ser invocadas a través de la línea de comandos, una IU web integrada o mediante la API expuesta por la plataforma. 
OpenFaaS tiene una excelente compatibilidad para métricas y ofrece autoescalado para funciones cuando se incrementa la demanda.


#### Kubernetes
Kubernetes es un sistema de código libre para la automatización del despliegue, ajuste de escala y manejo de aplicaciones en contenedores que fue originalmente diseñado por Google y donado a la Cloud Native Computing Foundation (parte de la Linux Foundation). Soporta diferentes entornos para la ejecución de contenedores, incluido Docker.

Podemos pensar en Kubernetes como:

- una plataforma de contenedores
- una plataforma de microservicios
- una plataforma portable de nube

Kubernetes ofrece un entorno de administración centrado en contenedores. Kubernetes orquesta la infraestructura de cómputo, redes y almacenamiento para que las cargas de trabajo de los usuarios no tengan que hacerlo. Esto ofrece la simplicidad de las Plataformas como Servicio (PaaS) con la flexibilidad de la Infraestructura como Servicio (IaaS) y permite la portabilidad entre proveedores de infraestructura



### Requerimientos

- Instalar Docker
- Instalar OpenFaaS CLI
- Registrar una cuenta de DockerHub
- Establecer el prefijo OpenFaas para las nuevas imágenes
- Instalar un cluster de Kubernetes o Docker Swarm 


#### Instalar Docker

En mi caso, la instalación va a ser sobre ubuntu: https://docs.docker.com/engine/install/ubuntu/

Desinstalar versiones anteriores:
```bash
$ sudo apt-get remove docker docker-engine docker.io containerd runc
```

Instalar docker vía repositorio:

1. Actualizar el índice de paquetes de apt e installar los paquetes necesarios para permitir que apt utilice un repositorio sobre HTTPS:
```bash
$ sudo apt-get update
```
```bash
$ sudo apt-get install \
    apt-transport-https \
    ca-certificates \
    curl \
    gnupg-agent \
    software-properties-common
```
2. Agregar la key GPG oficial de Docker:

```bash
$ curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo apt-key add -
```

3. Agregar el repositorio **stable**

```bash
$ sudo add-apt-repository \
   "deb [arch=amd64] https://download.docker.com/linux/ubuntu \
   $(lsb_release -cs) \
   stable"
```

4. Actualizar el índice de paquetes de apt e installar la última versión de **Docker Engine** y **containerd**

```bash
 $ sudo apt-get update
 $ sudo apt-get install docker-ce docker-ce-cli containerd.io
 ```
5. Verificar que Docker Engine esté correctamente instalado

```bash
sudo docker run hello-world
```
Este comando descarga una imagen de prueba y la corre en un container. Cuando el container es ejecutado, muestra por pantalla un mensaje informativo y finaliza.

#### Instalar OpenFaas CLI

```bash
$ curl -sLSf https://cli.openfaas.com | sudo sh
```
Probar el comando faas-cli
```bash
$ faas-cli help
$ faas-cli version
```

#### Registrar una cuenta en Docker Hub
Crear una cuenta en Docker Hub, y luego ingresar a la misma desde la terminal:
```bash
$ docker login
```

#### Establecer el prefijo OpenFaas para las nuevas imágenes

Editar el archivo ~/.bashrc or ~/.bash_profile - (crear el archivo si no existe).
Agregue al archivo la siguiente línea 

```bash 
export OPENFAAS_PREFIX="" # Completar con su nombre de usuario de Docker Hub,por ejemplo, export OPENFAAS_PREFIX="emiliolalvarez"
```

#### Instalar un cluster de Kubernetes o Docker Swarm
En pos de facilitar las pruebas locales, he optado por instalar un single-node cluster (cluster de un nodo) de Kubernetes.
Para lograr esto Kubernetes nos ofrece una servicio llamado **minikube** y una herrramienta de línea de comandos llamada **kubectl**.

##### Instalar kubectl
> **Importante:** la versión a descargar de kubectl tiene que ser igual a la versión del cluster de kubernetes que vayamos a utilizar o igual a la minor version anterior o siguiente inmediata. Es decir, la versión 1.12 de kubectl, va a poder operar con las versiones 1.11, 1.12 y 1.13 master del node de kubernetes.

1. Bajar la última versión de kubctl o una versión específica.
    
    Para bajar la última versión de kubectl:
    ```bash
    curl -LO https://storage.googleapis.com/kubernetes-release/release/`curl -s https://storage.googleapis.com/kubernetes-release/release/stable.txt`/bin/linux/amd64/kubectl
    ```
    Para bajar una versión específica, por ejemplo la 1.18.0, utilizaremos una url que denote el número de versión deseada:
    ```bash
    curl -LO https://storage.googleapis.com/kubernetes-release/release/v1.18.0/bin/linux/amd64/kubectl
    ```
2. Hacer ejecutable el binario kubectl
    ```bash
    chmod +x ./kubectl
    ```
3. Mover el binario descargado a un directorio incluído en PATH.
    ```bash
    sudo mv ./kubectl /usr/local/bin/kubectl
    ```
4. Verificar que la versión instalada esté actualizada.
    ```bash
    kubectl version --client
    ```
##### Instalar minikube

1. Verificar si la virtualización está habilitada en nuestro sistema:
    ```bash
    grep -E --color 'vmx|svm' /proc/cpuinfo
    ```
    Correr el comando y simplimente verificar que tenga output.

2. Asegurarse de haber instalado **kubectl**

3. Instalar un **hypervisor** como puede ser **KVM** o **Virtualbox**

4. Instalar Minikube vía descarga directa:
    ```bash
    curl -Lo minikube https://storage.googleapis.com/minikube/releases/latest/minikube-linux-amd64 && chmod +x minikube
    ```
5. Agregar el ejecutable de Minikube a nuestro PATH.

    ```bash
    sudo mkdir -p /usr/local/bin/
    sudo install minikube /usr/local/bin/
    ```
6. Confirmar la instalación.
   
    ```bash
    minikube start --driver=<driver_name> 
    ```
    > **Nota:** driver_name es nombre del hypervisor elegido, por ejemplo **virtalbox**

    Una vez que el proceso minikube start termine, correr el siguiente comando para chequear el estado del cluster:

    ```bash
    minikube status
    ```
    Esto generará un output similar al siguiemte:

    ```bash
    host: Running
    kubelet: Running
    apiserver: Running
    kubeconfig: Configured
    ```
    After you have confirmed whether Minikube is working with your chosen hypervisor, you can continue to use Minikube or you can stop your cluster. To stop your cluster, run:
    Después de haber confirmado que Minikube está funcionando con el hypervisor elegido, podemos proserguir a utlizar el cluster o pararlo.
    Para parar el cluster ejecutamos:
    ```bash
    minikube stop
    ```

7. Limpiar estado local.
  
    Si ya teníamos una versión previa instalada de minikube y al correr **minukube start** nos arroja el siguiente mensaje:
    ```bash
    machine does not exist
    ```
    Corremos luego, el siguiente comando:
    ```bash
    minikube delete
    ```
#### Instalar el cliente faas (faas-cli)
Podemos instalar el cliente mediante curl
```bash
curl -sSL https://cli.openfaas.com | sudo sh
```
#### Instalar OpenFaas en Kubernetes
Tenemos tres posibiliadades:
* arkade (Es un cliente escrito en Golang para instalar charts y programas, es la opción más rápida y simple)
* helm chart (Helm es el administrador de paquetes de Kubernetes)
* plain yml files

> Nota: un **chart** de helm es una colección de archivos que describe un conjunto de recursos de Kubernetes.
> Un **pod** es la unidad desplegable mas pequeña que puede ser creada y administrada por Kubernetes.
> Un chart puede ser utlizado para algo simple como desplegar por ejemplo un pod de memcached, o para algo complejo
> como deployar una aplicación web entera, con servidores http, bases de datos, etc

Para facilitar las pruebas utlizaremos para instalar OpenFaas la versión basada en Arkade.

El comando de instalación arkade instala OpenFaaS utilizando su helm chart oficial, pero sin utlizar tiller, un componente que es inseguro por default. Arkade puede instalar además otros softwares importantes para los usuarios de OpenFaas como pueden ser **cert-manager** y **nginx-ingress**. Es la forma mas sencilla de levantar y correr OpenFaas.

1. Descargar e instalar arkade.
    ```bash
    curl -SLsf https://dl.get-arkade.dev/ | sudo sh
    ``` 
2. Instalar OpenFaas en nuestro cluster local
    ```bash
    arkade install openfaas
    ```
3. Luego de ejecutar la instalación se nos mostrará un comando para obtener la **url OpenFaas** donde va a estar disponible el front-end del servicio, otro comando
para obtener la **contraseña** y las instrucciones para desplegar el gateway de OpenFaas y exponer el puerto donde va a estar corriendo el servicio web.

    ```bash
    =======================================================================
    = OpenFaaS has been installed.                                        =
    =======================================================================

    # Get the faas-cli
    curl -SLsf https://cli.openfaas.com | sudo sh

    # Forward the gateway to your machine
    kubectl rollout status -n openfaas deploy/gateway
    kubectl port-forward -n openfaas svc/gateway 8080:8080 &

    # If basic auth is enabled, you can now log into your gateway:
    PASSWORD=$(kubectl get secret -n openfaas basic-auth -o jsonpath="{.data.basic-auth-password}" | base64 --decode; echo)
    echo -n $PASSWORD | faas-cli login --username admin --password-stdin
    ```
    
    La url por defecto es **http://127.0.0.1:8080/ui/** .
    Previamente, para acceder a dicha url tenemos que desplegar el gateway OpenFaas y hacer un port-foward del puerto 8080 que utiliza el servicio OpenFaas
    ```bash
    # Forward the gateway to your machine
    kubectl rollout status -n openfaas deploy/gateway
    kubectl port-forward -n openfaas svc/gateway 8080:8080 &
    ```
    Podemos acceder utilizando el usuario **admin** cuyo password por defecto es **82F6vC8Tt2zdgrK1374RoqpU5** .

#### El cliente faas-cli

Podemos ver los comandos disponibles haciendo:

```bash
faas-cli --help

Manage your OpenFaaS functions from the command line

Usage:
  faas-cli [flags]
  faas-cli [command]

Available Commands:
  auth           Obtain a token for your OpenFaaS gateway
  build          Builds OpenFaaS function containers
  cloud          OpenFaaS Cloud commands
  completion     Generates shell auto completion
  deploy         Deploy OpenFaaS functions
  describe       Describe an OpenFaaS function
  generate       Generate Kubernetes CRD YAML file
  help           Help about any command
  invoke         Invoke an OpenFaaS function
  list           List OpenFaaS functions
  login          Log in to OpenFaaS gateway
  logout         Log out from OpenFaaS gateway
  logs           Tail logs from your functions
  namespaces     List OpenFaaS namespaces
  new            Create a new template in the current folder with the name given as name
  push           Push OpenFaaS functions to remote registry (Docker Hub)
  remove         Remove deployed OpenFaaS functions
  secret         OpenFaaS secret commands
  store          OpenFaaS store commands
  template       OpenFaaS template store and pull commands
  up             Builds, pushes and deploys OpenFaaS function containers
  version        Display the clients version information

Flags:
      --filter string   Wildcard to match with function names in YAML file
  -h, --help            help for faas-cli
      --regex string    Regex to match with function names in YAML file
  -f, --yaml string     Path to YAML file describing function(s)

Use "faas-cli [command] --help" for more information about a command.
```
Para listar las funciones actualmente disponibles en nuestra intancia de OpenFaas:
```bash
faas-cli list

Function                      	Invocations    	Replicas
base64                        	0              	1    
cows                          	0              	1    
echoit                        	0              	1    
figlet                        	0              	1    
hubstats                      	0              	1    
markdown                      	0              	1    
nodeinfo                      	0              	1    
qrcode-go                     	0              	1    
text-to-speech                	0              	1    
wordcount                     	0              	1 
```

##### Invocar una función utilizando faas-cli

```bash
echo "Hola" | faas-cli invoke figlet 

| | | | ___ | | __ _ 
| |_| |/ _ \| |/ _` |
|  _  | (_) | | (_| |
|_| |_|\___/|_|\__,_|
```

Otra forma de invocar la función es haciendo directamente:

```bash
faas-cli invoke figlet
```

Y luego se nos pedirá de dar un input:

```bash
Reading from STDIN - hit (Control + D) to stop.
```
Escribimos por ejemplo "Hola Mundo" y presionamos las teclas Control + D
```bash
Hola Mundo
```
Obtendremos el output de la función:

```bash
 _   _       _         __  __                 _      
| | | | ___ | | __ _  |  \/  |_   _ _ __   __| | ___  
| |_| |/ _ \| |/ _` | | |\/| | | | | '_ \ / _` |/ _ \ 
|  _  | (_) | | (_| | | |  | | |_| | | | | (_| | (_) |
|_| |_|\___/|_|\__,_| |_|  |_|\__,_|_| |_|\__,_|\___/                                                       
```
#### Crear nuevas funciones

Para crear una nueva función, por ejemplo una escrita en php y que llamaremos **testphp** ejecutamos

$ faas-cli new --lang php7 testphp --prefix=emiliolalvarez

> Nota: --prefix debemos poner nuestro nombre de usuario de dockerhub

El comando anterior creará una esctructura de directorio basada en el template de php7.
Está estructura generada, contendrá un Handler para ejecutar nuestro código php y administrar dependencias con composer. 
También contendrá un docker file para generar el container que necesitará nuestra función para poder ejecutarse.

> Nota: es necesario estar logueado en docker hub para poder pushear la imagen del contenedor de nuestra función.

```bash
docker login
```

Para buildear, pushear y desplegar la función ejecutaremos los siguientes comandos:
```bash
faas-cli build -f testphp.yml
faas-cli push -f testphp.yml  
faas-cli deploy -f testphp.yml
```

Durante el build se arma la imagen que será el contenedor de nuestra función. En este caso, como usamos el template de php, de fondo 
estaremos usando una imagen de docker basada en Alpine.
El push, si estamos logueados a docker hub, sube la imagen generada a nuestro repo.
Y el deploy finalmente despliega nuestra función en el servidor OpenFaas.


Alternativamente podemos usar:

```bash
faas-cli up -f testphp.yml  
```
El comando faas-cli up hace automáticamente el build, el push y el deploy.
Si listamos nuevamente las funciones de nuestra instancia, aparecerá **testphp** entre las funciones disponibles
```bash
faas-cli list

Function                      	Invocations    	Replicas
base64                        	0              	1    
cows                          	0              	1    
echoit                        	0              	1    
figlet                        	5              	1    
hubstats                      	0              	1    
markdown                      	0              	1    
nodeinfo                      	0              	1    
qrcode-go                     	0              	1    
testphp                       	1              	1    
text-to-speech                	2              	1    
wordcount                     	0              	1   
```

Para editar las extensiones de php/ paquetes apk (administrador de paquetes de Alpine Linux) que querramos instalar, editamos el archivo **testphp/php-extension.sh** 

Su contenido será algo como:

```bash
#!/bin/sh

echo "Installing PHP extensions"

# Add your extensions in here, an example is below
# docker-php-ext-install mysqli
#
# See the template documentation for instructions on installing extensions;
# - https://github.com/openfaas/templates/tree/master/template/php7
#
# You can also install any apk packages here
~                   
```

Si queremos por ejemplo instalar una aplicación vía apk lo podemos hacer agregando el comando de instalación en este archivo.
```bash
#!/bin/sh

echo "Installing PHP extensions"

# Add your extensions in here, an example is below
# docker-php-ext-install mysqli
#
# See the template documentation for instructions on installing extensions;
# - https://github.com/openfaas/templates/tree/master/template/php7
#
# You can also install any apk packages here
#
# Instalamos el programa fortune, que devuelve una frase, como si fuese una galleta de la fortuna. 
apk add fortune 
~                   
```


Ahora modifiquemos el handler de php para que cuando la función sea invocada
devuelva una frase random basándose en el output del comando **fortune**

Editamos el archivo **testphp/src/Handler.php**

```bash
vim testphp/src/Handler.php
```

```php

<?php

namespace App;

/**
 * Class Handler
 * @package App
 */
class Handler
{
    /**
     * @param $data
     * @return
     */
    public function handle($data) {
    	//Devolvemos el output de la invocación al comando fortune
        return shell_exec('fortune');
    }
}

```

Debemos buildear y desplegar la función nuevamente:

```bash
faas-cli up -f testphp.yml
```


Ahora invocamos la función:

```bash
echo "" | faas-cli invoke testphp
``` 
Y el output será el siguiente:

```bash
Handling connection for 8080
One, with God, is always a majority, but many a martyr has been burned
at the stake while the votes were being counted.
		-- Thomas B. Reed
```


### Stats con Grafana (metrics dashboard)
Grafana es una aplicación web multiplataforma y de código abierto para la visualización de datos en serie temporales. A partir de una serie de datos recolectados obtendremos un panorama gráfico de los mismos. Provee de cuadros, gráficos y alertas para la web cuando esta contectado a fuentes de datos soprtadas. Es extensible vía un sistema de plugins. Los usuarios pueden crear dashboards de monitoreo complejos usando query builders interactivos.

OpenFaas puede integrarse con Grafana para tener métrica visuales de las invocaciones a las distintas funciones en los distintos períodos de tiempo.

Para instalar la integración con Grafana, primero corremos en nuestro nodo de Kubernetes la imagen que provee dicha funcionalidad.

```bash
kubectl -n openfaas run --generator=run-pod/v1 --image=stefanprodan/faas-grafana:4.6.3 --port=3000 grafana
```	  
Luego exponemos el puerto de la aplicación web de Grafana para que sea accesible.
```bash
kubectl -n openfaas expose pod grafana --type=NodePort --name=grafana
```

Por defecto Grafana es expuesto en el puerto **31929** del nodo de Kubernetes
Si no recordamos la ip del nodo podemos recuperarla con el siguiente comando:
```bash
minikube ip
```
Esto nos devolverá la ip de nuestro nodo minikube
```bash
192.168.32.2
```

La url de la aplicación web de Grafana será entonces:
```bash
http://192.168.32.2:31929/login
```

Por defecto las credenciales para acceder al dashboard de Grafana son:
```bash
user: admin
pass: admim
```

#### Monitorear el funcionamiento de OpenFaas en Grafana con una simple prueba de performance
Para tener algún input interesante para hacer una prueba sencilla del comportamiento de OpenFaas, a medida que va recibiendo los requests de las distintas 
funciones podríamos hacer invocaciones concurrentes a las mismas y ver como se refleja esa información en el dashboard de Grafana.
Para poder simular una gran cantindad de requests y algo de simultaneidad en los mismos, podemos utilizar el comando **ab**
El comando **ab** (Apache Benchmark) es una herramienta desarrollada por Apache y sirve para hacer pruebas de carga a un servidor. 
Si no lo tenemos instaldo es parte del paquete **apache2-utils**. 
Podemos instalarlo con:

```bash
sudo apt install apache2-utils
```

Una vez instalado, podremos realizar pruebas de concurrencia sobre nuestra instalación de OpenFaas.
Por ejemplo podemos probar la ejecución de 1000 requests sobre la función **testphp** desarrollada anteriormente, indicando una concurrencia de no mas de 50 requests simultáneos.

```bash
ab -n 1000 -c 50 http://127.0.0.1:8080/function/testphp
```
Podemos ver en tiempo real las métricas visuales en el dashboard de Grafana.


![Grafana OpenFaas](ASSET/grafana-openfaas.png)
